package logging

import (
	"github.com/getsentry/sentry-go"
	"github.com/rs/zerolog"
)

type sentryHook struct{}

func (h sentryHook) Run(e *zerolog.Event, level zerolog.Level, msg string) {
	var sl sentry.Level
	switch level {
	case zerolog.ErrorLevel:
		sl = sentry.LevelError
	case zerolog.WarnLevel:
		sl = sentry.LevelWarning
	default:
		return
	}

	event := sentry.NewEvent()
	event.Message = msg
	event.Level = sl
	sentry.CaptureEvent(event)
}
