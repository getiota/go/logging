// Package logging makes it easier to deal with zerolog and sentry
package logging

import (
	"os"
	"strconv"

	"github.com/rs/zerolog"

	"github.com/getsentry/sentry-go"
	"github.com/rs/zerolog/log"

	sentryecho "github.com/getsentry/sentry-go/echo"
	"github.com/labstack/echo/v4"
)

func setLogLevelFromEnv() {
	level, found := os.LookupEnv("LOG_LEVEL")
	if !found {
		return
	}

	lInt, err := strconv.Atoi(level)
	if err != nil {
		log.Warn().Msgf("Invalid log level '%s'", level)
		return
	}

	lZero := zerolog.Level(lInt)
	if lZero.String() == "" {
		log.Warn().Msgf("Unkwown log level '%d'", lZero)
		return
	}
	log.Info().Msgf("Set log level '%s'", lZero.String())
	zerolog.SetGlobalLevel(lZero)
}

// PrepareWithSentry allows customizing the sentry client options
func PrepareWithSentry(e *echo.Echo, sentryOptions *sentry.ClientOptions) {
	setLogLevelFromEnv()

	// Forwarding all Echo logs to default zerolog logger
	e.Logger = EchoLogger{}

	// Using Zerolog Middleware
	e.Use(Middleware())

	if sentryOptions != nil {
		// Setting up sentry middleware
		if err := sentry.Init(*sentryOptions); err != nil {
			log.Warn().Err(err).Msg("Sentry Init failed")
			return
		}
		// Adding Echo Middleware
		e.Use(sentryecho.New(sentryecho.Options{}))
		// Adding sentry hook to default Logger
		log.Logger = log.Logger.Hook(sentryHook{})
	} else {
		log.Warn().Msg("Sentry will not be configured")
	}
}

// Prepare configures everything logging related
func Prepare(e *echo.Echo) {
	var sentryOptions *sentry.ClientOptions

	if sentryDsn, _ := os.LookupEnv("SENTRY_DSN"); sentryDsn != "" {
		sentryOptions = &sentry.ClientOptions{
			Dsn:         sentryDsn,
			Environment: os.Getenv("STAGE"),
		}
	}
	PrepareWithSentry(e, sentryOptions)
}
