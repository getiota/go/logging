# Logging

A simple utility to provide consistent logging accross all apps.

## Usage

Call the `Prepare` function at startup and dimply use [zerolog](https://github.com/rs/zerolog).

The prepare functions:
- register an echo middleware to log requests
- set a logger based on zerolog to the default Echo Logger
- if the `SENTRY_DSN` env var is defind, it adds a hook so that all errors are logged to sentry as well
